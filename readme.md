# TrackFeels

A quantified self, unhosted web app to let you track how you're feeling during particular activities.

## TODO imminently

* Hide subactivity form when main activity is deleted.
* Delete subactivities when main activity is deleted. 
* Figure out if I can order things?
* Linked Data it up.
* Stuff with emotions...

## TODO long term

* Be able to tag *everything*.
* Cool visualisations.