(function() {

  var today;
  var activityForm;
  var inputTitle;
  var inputDescr;
  var ulElement;
  var trackBox;
  var sAForm;
  var sATitle;
  var sADescr;
  var parentActivityId;
  var sAList;

  /********************************************/
  // General functions
  // Probably should put these somewhere else 
  /********************************************/

  function convert24To12Hour(date){
    if(typeof date === 'undefined'){ date = new Date(); }
    var hourStr;
    var hour = (date.getHours());
    if(hour == 12){
      hourStr = "12pm";
    }else if(hour == 0){
      hourStr = "12am";
    }else if(hour > 12){
      hourStr = (hour-12).toString() + "pm";
    }else{
      hourStr = hour.toString() + "am";
    }
    return hourStr;
  }

  /********************************************/
  // Init 
  /********************************************/

  function init() {

    // Get activity DOM elements
    today = (new Date()).toString().split(' ').splice(1,3).join(' ');
    activityForm = document.getElementById('aForm');
    inputTitle = document.getElementById('aTitle');
    inputDescr = document.getElementById('aDescr');
    ulElement = document.getElementById('aList');
    trackBox = document.getElementById('aTracker');
    // Get subActivity DOM elements
    sAForm = document.getElementById('sAForm');
    sATitle = document.getElementById('sATitle');
    sADescr = document.getElementById('sADescr');
    parentActivityId = document.getElementById('parentActivityId');
    sAList = document.getElementById('sAList');

    // Add current date to activity form
    inputTitle.placeholder = "Existing on " + today;
    // Hide activity-specific things
    trackBox.style.display = "none";

    // remoteStorage stuff
    remoteStorage.access.claim('trackfeels', 'rw');
    remoteStorage.displayWidget();
    remoteStorage.trackfeels.init();
    // Do things when data in the store changes.
    // ..There has got to be a better way of doing this?
    remoteStorage.trackfeels.on('change', function(event) {
      if(event.newValue && (! event.oldValue)) {
        // Update list
        if(!event.newValue.containedBy){
          displayActivity(event.newValue);
        }else{
          displaySubActivity(event.newValue);
        }
        addActivityClickListener(event.newValue.id);
        // TODO: Update subactivity list too
      }
    });

    // When RS has loaded:
    remoteStorage.on('features-loaded', function(){  

      // Make initial list of existing activities
      listActivities();

      // Store new activities when forms submitted
      activityForm.addEventListener('submit', function(event) {
        event.preventDefault();
        var newActivity = {};
        var trimmedTitle = inputTitle.value.trim();
        var trimmedDescr = inputDescr.value.trim();
        if(trimmedTitle) {
          newActivity.title = trimmedTitle;
          newActivity.description = trimmedDescr;
          addActivity(newActivity);
          inputTitle.value = "";
          inputDescr.value = "";
        }else{
          // Show error
        }
        
      });

      sAForm.addEventListener('submit', function(event){
        event.preventDefault();
        var newSubActivity = {};
        var trimmedTitle = sATitle.value.trim();
        var trimmedDescr = sADescr.value.trim();
        if(trimmedTitle) {
          newSubActivity.title = trimmedTitle;
          newSubActivity.description = trimmedDescr;
          newSubActivity.containedBy = parentActivityId.value;
          addActivity(newSubActivity);
          sATitle.value = "";
          sADescr.value = "";
          updateDisplayCount(parentActivityId.value, 1);
        }else{
          // Show error
        }
      });

    });

  }

  /********************************************/
  // Store new things
  /********************************************/

  // Store activity
  function addActivity(activity) {
    remoteStorage.trackfeels.addActivity(activity);
  }

  /********************************************/
  // Update things
  /********************************************/

  function startActivity(activityId){
    remoteStorage.trackfeels.startActivity(activityId).then(function(activity){
      displayActivityStarted(activity);
    });
  }

  /********************************************/
  // Delete things
  /********************************************/

  function deleteActivity(activityId){
    undisplayActivity(activityId);
    remoteStorage.trackfeels.deleteActivity(activityId);
    // TODO: Delete all subactivities
  }

  /********************************************/
  // Display / DOM stuff
  /********************************************/

  function listActivities(){
    var activitiesCounter = {};
    remoteStorage.trackfeels.getActivities().then(function(activities){

      // Loop through to count activities with parents.
      for (var key in activities) {
        if (activities.hasOwnProperty(key)) {
          if(activities[key].containedBy){
            // Count activities with parents.
            parentId = activities[key].containedBy;
            if(activitiesCounter[parentId]){
              // If parent is already a key in the array, increment.
              activitiesCounter[parentId] = activitiesCounter[parentId] + 1;
            }else{
              // Otherwise, create new key.
              activitiesCounter[parentId] = 1;
            }
          }else{
            activitiesCounter[key] = 0;
          }
        } //end if hasOwnProperty
      } //end for

      // Loop through again, to display this time
      for(var key in activities){
        if (activities.hasOwnProperty(key)) {
          displayActivity(activities[key], activitiesCounter[key]);
          addActivityClickListener(activities[key].id);
        }
      } //end for

    }); //end then
  }

  function listSubActivities(parentId){
    sAList.innerHTML = "";
    remoteStorage.trackfeels.getActivities().then(function(activities){
      for (var key in activities) {
        if (activities.hasOwnProperty(key)) {
          if(activities[key].containedBy == parentId){
            displaySubActivity(activities[key]);
            addActivityClickListener(activities[key].id);
          }
        }
      }
    });
  }

  function updateDisplayCount(id, i){
    var liE = document.getElementById(id);
    if(liE){
      var span = liE.getElementsByClassName('count')[0];
      var c = parseInt(span.innerText);
      span.innerHTML = c + i;
    }
  }

  function displayActivity(activity, childCount){
    var domId = activity.id;
    if(!activity.containedBy){
      var liE = document.getElementById(domId);
      if(!liE){
        liE = document.createElement('li');
        liE.id = domId;
        ulElement.appendChild(liE);  
      }
      liE.innerHTML = ""; // TODO: Find a more efficient way to fix the dup problem.
      liE.appendChild(document.createTextNode(activity.title + " "));
      var count = document.createElement('span');
      count.innerHTML = childCount;
      count.className = "count";
      liE.appendChild(count);
      var del = document.createElement('span');
      del.innerHTML = " [ x ]";
      del.className = "del"
      liE.appendChild(del);
    }
  }

  function displaySubActivity(activity){
    var domId = activity.id;
    var liE = document.getElementById(domId);
    if(!liE){
      liE = document.createElement('li');
      liE.id = domId;
      sAList.appendChild(liE);  
    }
    liE.innerHTML = ""; // TODO: Find a more efficient way to fix the dup problem
    liE.appendChild(document.createTextNode(activity.title));
    var del = document.createElement('span');
    del.innerHTML = " [ x ]";
    del.className = "del"
    liE.appendChild(del);
  }

  function undisplayActivity(activityId){
    liE = document.getElementById(activityId);
    if(liE){
      liE.parentNode.removeChild(liE);
    }
    parentId = document.getElementById('parentActivityId').value;
    if(parentId){
      updateDisplayCount(parentId, -1);
    }
  }

  function addActivityClickListener(id){
    var liE = document.getElementById(id);
    if(liE){
      var del = liE.getElementsByClassName('del')[0];
      liE.addEventListener('click', function(event){
        startActivity(id);
      });
      del.addEventListener('click', function(event){
        deleteActivity(event.target.parentNode.id);
        event.stopPropagation();
      });

    }
  }

  function makeTrackingBox(activity){
    var h = trackBox.getElementsByTagName('h3')[0];
    h.innerHTML = activity.title;
    trackBox.insertBefore(h,trackBox.firstChild);
    if(!activity.containedBy){
      sAForm.style.display = "block";
      updateSubActivityForm(activity); 
      listSubActivities(activity.id);
    }else{
      sAForm.style.display = "none";
    }
    trackBox.style.display = "block";
  }

  function updateSubActivityForm(activity){
    var today = new Date();
    var curHour = convert24To12Hour(today);
    today.setHours(today.getHours()+1);
    var nextHour = convert24To12Hour(today);
    var existing = "Existing between " + curHour + " and " + nextHour;
    var parentActivity = document.getElementById('parentActivityId');
    document.getElementById('sATitle').placeholder = existing;
    parentActivity.value = activity.id;
  }

  function displayActivityStarted(activity){
    // Make all list elements not bold
    var activities = document.getElementsByTagName('li');
    for(var i=0; i < activities.length; i++){
      activities[i].style.fontWeight = "normal";
    }
    // Make clicked element bold
    var active = document.getElementById(activity.id);
    active.style.fontWeight = "bold";
    // Make box with subactivity form etc in
    makeTrackingBox(activity);
  }



  document.addEventListener('DOMContentLoaded', init);

})();