RemoteStorage.defineModule('trackfeels', function(privateClient, publicClient) {

  function init() {
    //privateClient.cache('', true);
  }

  // TODO: Make this json-ld
  privateClient.declareType('sample', {
    "description": "a record of an emotion in relation to an activity",
    "type": "object",
    "properties": {
      "id": {
        "type": "string",
        "format": "id"
      },
      "EARLEmotionCategory": {
        "type": "string"
      },
      "wasGeneratedBy": {
        "type": "string",
        "format": "id"
      },
      "atTime": {
      	"type": "string"
      }
    }
  });

  privateClient.declareType('activity', {
    "description": "an activity (subClassOf prov:Activity)",
    "type": "object",
    "properties": {
      "id": {
        "type": "string",
        "format": "id"
      },
      "title": {
        "type": "string"
      },
      "description": {
        "type": "string"
      },
      "containedBy": {
        "type": "string",
        "format": "id"
      }
    }
  });

  return {
    exports: {

      init: init,
      on: privateClient.on,

      addActivity: function (obj) {
        var id = new Date().getTime().toString();
        obj.id = id;
        return privateClient.storeObject('activity', 'activities/'+id, obj);
      },

      getActivities: function(){
		    return privateClient.getAll('activities/');
      },

      startActivity: function(activityId){
      	return privateClient.getObject('activities/'+activityId);
      },

      deleteActivity: function(activityId){
        return privateClient.remove('activities/'+activityId);
      }

      // addSample
      // getSamples
    }
  };
});